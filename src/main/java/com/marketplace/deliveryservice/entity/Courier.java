package com.marketplace.deliveryservice.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;


/**
 * Сущность, Курьер.
 * id - уникальный идентификатор
 * name - имя
 * phone_number - номер телефона
 * city - город
 * delivery_count - оплата доставки
 * registration_date - дата регистрация
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "COURIER")
@Schema(description = "Курьер")
public class Courier {
    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "phone_number")
    @NotNull
    private String phoneNumber;

    @Column(name = "city")
    @NotNull
    private String city;

    @Column(name = "delivery_count")
    @NotNull
    private Long deliveryCount;

    @Column(name = "registration_date")
    @NotNull
    private LocalDateTime registrationDate;
}
