package com.marketplace.deliveryservice.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CourierDto {

    @Schema(example = "123e4567-e89b-12d3-a456-426614174000", description = "Уникальный идентификатор товара")
    @NotNull
    private UUID id;

    @Schema(example = "Имя", description = "Имя курьера")
    @NotNull
    private String name;

    @Schema(example = "Номер телефона", description = "Телефонный номер курьера")
    @NotNull
    private String phoneNumber;

    @Schema(example = "Город", description = "Город работы")
    @NotNull
    private String city;

    @Schema(example = "сумма", description = "Сумма доставки")
    @NotNull
    private Long deliveryCount;
}