package com.marketplace.deliveryservice.api.controller;



import com.marketplace.deliveryservice.api.dto.CourierDto;
import com.marketplace.deliveryservice.api.resource.CourierResource;
import com.marketplace.deliveryservice.api.util.CourierMapper;
import com.marketplace.deliveryservice.service.CourierService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@AllArgsConstructor
@RestController
public class CourierController implements CourierResource {

    private final CourierService courierService;

    private final CourierMapper courierMapper;


    @Override
    public ResponseEntity<CourierDto> findByCourierId(UUID id) {
        return ResponseEntity.ok(courierMapper.convertItemToDto(courierService.findCourierById(id)));
    }
}
